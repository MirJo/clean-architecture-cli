const splitFeatureName = function (featureName: string): string[] {
    const indizesOfUpperCaseLetters = [0];
    const resultArray = [];

    // find idizes of upper case letters
    for (let index = 0; index < featureName.length; index++) {
        if (isUpperCase(featureName[index])) {
            indizesOfUpperCaseLetters.push(index);
        }
    }

    indizesOfUpperCaseLetters.push(featureName.length);

    // push substrings of each lower case word into array
    for (let index = 0; index < indizesOfUpperCaseLetters.length - 1; index++) {
        resultArray.push(featureName.substring(indizesOfUpperCaseLetters[index], indizesOfUpperCaseLetters[index + 1]).toLowerCase());
    }


    return resultArray;
}

const isUpperCase = function (letter: string): boolean {
    return letter == letter.toUpperCase() && letter != letter.toLowerCase();
}


export { splitFeatureName }