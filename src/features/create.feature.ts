import path = require('path');
import fs = require('fs');
import { Framework } from './data.types';

const createFolderStructure = function (featureName: string[], projectPath: string, framework: Framework): boolean {
    try {
        if (framework === Framework.ANGULAR) {
            const dirApp = path.join(projectPath, 'src', 'app');
            const dirFeature = path.join(dirApp, 'features');

            createDirIfNotExists(dirFeature);
            const featureFolderName = featureName.join('-');

            // create feature folder
            createDirIfNotExists(path.join(dirFeature, featureFolderName));

            //create data-source folder
            createDirIfNotExists(path.join(dirFeature, featureFolderName, 'data-source'));

            //create repository folder
            createDirIfNotExists(path.join(dirFeature, featureFolderName, 'repository'));

            //create use case folder
            createDirIfNotExists(path.join(dirFeature, featureFolderName, 'use-case'));

        }
    } catch (e) {
        return false;
    }
    return true;
}

const createFiles = function (featureName: string[], projectPath: string, framework: Framework): boolean {



    try {
        if (framework === Framework.ANGULAR) {
            const dirFeature = path.join(projectPath, 'src', 'app', 'features', featureName.join('-'));
            const featureFileNameTemplate = featureName.join('.');

            const inFileNameTemplate = featureName.map(el => el[0].toUpperCase() + el.substring(1)).join('');


            const dataSourceFileName = `${featureFileNameTemplate}.data.source.ts`;
            const dataSourceImportName = `${featureFileNameTemplate}.data.source`;
            const dataSourceClassName = `${inFileNameTemplate}DataSource`;

            const repoFileName = `${featureFileNameTemplate}.repository.ts`;
            const repoImportName = `${featureFileNameTemplate}.repository`;
            const repoClassName = `${inFileNameTemplate}Repository`;

            const useCaseFileName = `${featureFileNameTemplate}.use.case.ts`;
            const useCaseImportName = `${featureFileNameTemplate}.use.case`;
            const useCaseClassName = `${inFileNameTemplate}UseCase`;


            // #region Text literals
            const moduleContent = `
import { NgModule } from '@angular/core';
import { Abstract${dataSourceClassName} } from './data-source/abstract.${dataSourceImportName}';
import { ${dataSourceClassName} } from './data-source/${dataSourceImportName}';
import { Abstract${repoClassName} } from './repository/abstract.${repoImportName}';
import { ${repoClassName} } from './repository/${repoImportName}';
import { Abstract${useCaseClassName} } from './use-case/abstract.${useCaseImportName}';
import { ${useCaseClassName} } from './use-case/${useCaseImportName}';    
@NgModule({
    providers: [
        {
            provide: Abstract${useCaseClassName},
            useClass: ${useCaseClassName}
        },
        {
            provide: Abstract${repoClassName},
            useClass: ${repoClassName}
        },
        {
            provide: Abstract${dataSourceClassName},
            useClass: ${dataSourceClassName}
        }
    ]
})
export class ${inFileNameTemplate}Module { }`;

            const abstractDataSourceContent = `export abstract class Abstract${dataSourceClassName} {
    abstract method(): Promise<any>;
}`;

            const dataSourceContent = `import { Injectable } from '@angular/core';
import {Abstract${dataSourceClassName}} from './abstract.${dataSourceImportName}';

@Injectable()
export class ${dataSourceClassName} extends Abstract${dataSourceClassName} {
    async method(): Promise<any> {
        //TODO: Create Method
        console.error('no datasource implemented');
        return undefined;
    }
}`;

            const abstractRepoContent = `export abstract class Abstract${repoClassName} {
    abstract method(): Promise<any>;
}`;

            const dataRepoContent = `import { Injectable } from '@angular/core';
import {Abstract${repoClassName}} from './abstract.${repoImportName}';
import {Abstract${dataSourceClassName}} from '../data-source/abstract.${dataSourceImportName}';


@Injectable()
export class ${repoClassName} extends Abstract${repoClassName} {
    constructor(private dataSource: Abstract${dataSourceClassName}) { super(); }
    async method(): Promise<any> {
        try {
            const result = await this.dataSource.method();
            console.error('no repository implemented');
            return result;
        } catch (e) {
            console.error('no repository implemented');
        }
    }
}`;

            const abstractUseCaseContent = `export abstract class Abstract${useCaseClassName} {
    abstract execute(param?: any): Promise<any>;
}`;

            const dataUseCaseContent = `import { Injectable } from '@angular/core';
import {Abstract${useCaseClassName}} from './abstract.${useCaseImportName}';
import {Abstract${repoClassName}} from '../repository/abstract.${repoImportName}';


@Injectable()
export class ${useCaseClassName} extends Abstract${useCaseClassName} {
constructor(private repository: Abstract${repoClassName}) { super(); }
    execute(param?: any): Promise<any> {
        console.error('no use case implemented');
        return this.repository.method();
    }
}`;
            //#endregion

            // module ts


            createAndWriteIntoFile(path.join(dirFeature, featureFileNameTemplate + '.module.ts'), moduleContent);

            // data source files

            createAndWriteIntoFile(path.join(dirFeature, 'data-source', 'abstract.' + dataSourceFileName), abstractDataSourceContent);
            createAndWriteIntoFile(path.join(dirFeature, 'data-source', dataSourceFileName), dataSourceContent);

            // repository files

            createAndWriteIntoFile(path.join(dirFeature, 'repository', 'abstract.' + repoFileName), abstractRepoContent);
            createAndWriteIntoFile(path.join(dirFeature, 'repository', repoFileName), dataRepoContent);

            // use case files

            createAndWriteIntoFile(path.join(dirFeature, 'use-case', 'abstract.' + useCaseFileName), abstractUseCaseContent);
            createAndWriteIntoFile(path.join(dirFeature, 'use-case', useCaseFileName), dataUseCaseContent);



        }
    } catch (e) {
        return false;
    }
    return true;
}


const createDirIfNotExists = function (path: string) {
    if (!fs.existsSync(path)) {
        fs.mkdirSync(path);
    }
}

const createAndWriteIntoFile = function (filename: string, content = '') {

    fs.writeFile(filename, content, { flag: 'w' }, function (err) {
        if (err) throw err;
        console.log(`${path.basename(filename)} created`);
    });
}

export { createFolderStructure, createFiles }