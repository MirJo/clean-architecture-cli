#!/usr/bin/env node
import { Command } from 'commander';
import { createFolderStructure, createFiles } from './features/create.feature';
import { Framework } from './features/data.types';
import { splitFeatureName } from './features/utils';
import path = require('path');
// import * as packageJson from '../package.json';
const chalk = require('chalk');
const clear = require('clear');
const figlet = require('figlet');

clear();
console.log(
    chalk.blue(
        figlet.textSync('clean architecture-cli', { horizontalLayout: 'full' })
    )
);

const program = new Command();
program.version('0.1.0', '-v, --version', 'output the current version')
    .name('cleanarch')
    .usage("[options] command")
    .description('Add folders and files for you clean architecture project')
    .option('-h, --help', 'Shows this menu');

program.command('feature <featureName> [projectPath]')
    .description('creates the folder structure, abstract files and empty implementations')
    .action((featureName, projectPath) => {
        if (!projectPath) {
            projectPath = __dirname
        }

        const subStrFeatureName = splitFeatureName(featureName);

        const createFolderDidWork = createFolderStructure(subStrFeatureName, projectPath, Framework.ANGULAR);
        if (!createFolderDidWork) {
            throw new Error("Folders could not be created");
        }
        console.log('---------');
        console.log('All folders created');
        console.log('---------');

        const createFilesDidWork = createFiles(subStrFeatureName, projectPath, Framework.ANGULAR);
        if (!createFilesDidWork) {
            throw new Error("Files could not be created");
        }
        console.log('---------');
        console.log('All files created');



    });


program.parse(process.argv);


const options = program.opts();

if (process?.argv) {
    console.log(process.argv);
}

if (!process.argv.slice(2).length) {
    program.outputHelp();
}